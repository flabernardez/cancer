<?php

get_header(); ?>

    <main id="content" class="site-content search-page">

	    <?php
	    if ( have_posts() ) :

			    // Start the Loop.
			    while ( have_posts() ) : the_post(); ?>


				    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <header class="entry-header">

						    <?php
						    if ( is_sticky() && is_home() && ! is_paged() ) {
							    printf( '<span class="sticky-post">%s</span>', _x( 'Featured', 'post', 'cancer-theme' ) );
						    }
						    the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
						    ?>

                        </header><!-- .entry-header -->

                        <section class="entry-content">

                            <?php the_excerpt(); ?>

                        </section><!-- .entry-content -->

                    </article><!-- #post-<?php the_ID(); ?> -->

		    <?php
            endwhile;

		    the_posts_navigation();

        else : ?>

                <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentynineteen' ); ?></p>
                <?php get_search_form(); ?>


        <?php

	    endif;

	    ?>

    </main><!-- #content -->

<?php

get_footer(); ?>