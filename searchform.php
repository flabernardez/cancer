<?php
/**
 * Template for displaying search forms in Cancer Theme
 *
 * @package WordPress
 * @subpackage Cancer_Theme
 * @since Cancer Theme 1.1
 */
?>
<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php ' ' . _x( 'Search for:', 'label' );?></span>
		<input type="search" class="search-field" placeholder="<?php esc_attr_e( 'Search for...', 'cancer-theme' ) . '" value="' . get_search_query()?>" name="s" />
	</label>
	<input type="submit" class="search-submit fas fa-search" value="" />
</form>

