<?php

if ( ! function_exists( 'taurus_theme_setup' ) ) :

	function cancer_theme_setup() {

		load_theme_textdomain( 'cancer-theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_post_type_support( 'page', 'excerpt' );

		add_filter('jpeg_quality', function($arg){return 100;});

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Header', 'cancer-theme' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'cancer-theme' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 60,
			'width'       => 180,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'cancer_theme_setup' );

function cancer_theme_scripts() {

	wp_enqueue_style( 'cancer-theme-style', get_stylesheet_uri() );

	wp_deregister_script( 'cancer-theme-jquery');
	wp_enqueue_script( 'cancer-theme-jquery', get_template_directory_uri() . '/js/jquery-min.js', array(), null, true);
	wp_enqueue_script( 'cancer-theme-navigation', get_template_directory_uri() . '/js/navigation-min.js', array(), '1', true );
	wp_enqueue_script( 'cancer-theme-customize', get_template_directory_uri() . '/js/customize-min.js', array(), '1', true );
	//wp_enqueue_script( 'cancer-theme-assets-event-move', get_template_directory_uri() . '/js/jquery.event.move-min.js', array(), '1', true );
	//wp_enqueue_script( 'cancer-theme-assets-twentytwenty', get_template_directory_uri() . '/js/jquery.twentytwenty-min.js', array(), '1', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cancer_theme_scripts', 99 );

add_theme_support( 'align-wide' );

add_theme_support( 'wp-block-styles' );

add_theme_support( 'responsive-embeds' );

add_theme_support('editor-styles');

add_editor_style( 'style-editor.css' );

// Widgets areas support
function cancer_theme_widgets_init() {

	register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'cancer-theme' ),
		'id'            => 'footer-widget',
		'description'   => esc_html__( 'Add widgets here.', 'cancer-theme' ),
		'before_widget' => '<article id="footer-widget-%1$s" class="footer-widget">',
		'after_widget'  => '</article>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'cancer_theme_widgets_init' );

// Add search widget to header nav
function cancer_theme_add_last_nav_item($items, $args) {
	// If this isn't the primary menu, do nothing
	if( !($args->theme_location == 'menu-1') )
		return $items;
	// Otherwise, add search form
	return '<li>' . get_search_form(false) . '</li>' . $items;
}
add_filter( 'wp_nav_menu_items', 'cancer_theme_add_last_nav_item', 10, 2 );

// Remove scripts from head
function move_scripts_from_head_to_footer() {
	remove_action( 'wp_head', 'wp_print_scripts' );
	remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
	remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

	add_action( 'wp_footer', 'wp_print_scripts', 5);
	add_action( 'wp_footer', 'wp_enqueue_scripts', 5);
	add_action( 'wp_footer', 'wp_print_head_scripts', 5);
}
add_action('wp_enqueue_scripts', 'move_scripts_from_head_to_footer');

function force_jquery_to_footer() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), false, NULL, true );
	wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'force_jquery_to_footer' );

// Remove jQuery from old wp_print_scripts
function remove_jquery_from_wp_print_scripts() {
	wp_deregister_script( 'jquery' );
}
add_action( 'wp_print_scripts', 'remove_jquery_from_wp_print_scripts' );

