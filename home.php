<?php

get_header(); ?>

	<main id="content" class="site-content">

		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="post-header">
                        <a href="<?php the_permalink();?>" title="<?php the_title(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </header>

                    <section class="post-content">
                        <span class="post-content-meta"><?php the_date();?></span>
                        <p class="post-content-text">
                            <?php the_excerpt(); ?>
                        </p>
					</section>

                </article><!-- #post-<?php the_ID(); ?> -->

			<?php
			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</main><!-- #content -->

<?php

get_footer(); ?>