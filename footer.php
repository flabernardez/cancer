
<footer id="colophon" class="site-footer">

	<article class="wrapper">

		<section class="footer-site-name">
            <?php

            $url = get_site_url( null, "", "https");
		    echo str_replace( 'https://', ' ', $url);

		    ?>
        </section>

        <?php
		if ( is_active_sidebar( 'footer-widget' ) ) : ?>

            <section class="footer-widget-area">
				<?php dynamic_sidebar( 'footer-widget' ); ?>
			</section>

		<?php
		endif; ?>

		<nav class="footer-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-2',
				'menu_id'        => 'footer-menu',
			) );
			?>
		</nav><!-- #site-navigation -->

	</article>

</footer><!-- #colophon -->

<?php wp_footer(); ?>

<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "#F2F4F4",
                    "text": "#4d4d4d"
                },
                "button": {
                    "background": "#00b4a4",
                    "text": "#ffffff"
                }
            },
            "theme": "edgeless",
            "content": {
                "message": "La información sobre las cookies que usamos, cómo desactivarlas, acceso y borrado de datos está en nuestra página de",
                "dismiss": "¡Vale!",
                "link": "Aviso Legal",
                "href": "http://empoderacine.local/aviso-legal/"
            }
        })});
</script>
</body>
</html>
