��          �      L      �  "   �     �     �     �  =     	   N     X     a     h  	   o  !   y     �     �     �     �  \   �  \   -     �  �  �  &   p     �     �     �  F   �  	     	        $     3     <  #   N  	   r     |     �      �  �   �  h   .     �                                   
                                	                                 %1$s thought on &ldquo;%2$s&rdquo; Add widgets here. Blog Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Error 404 Featured Footer Header Home page One thought on &ldquo;%1$s&rdquo; Pages: Search for... Search for: Search results for: Sorry, but nothing matched your search terms. Please try again with some different keywords. This page does not exist. Please, try to %1$s or we let you content that is already created: visit home page Project-Id-Version: Cancer Theme
POT-Creation-Date: 2019-01-23 10:41+0100
PO-Revision-Date: 2019-01-23 10:41+0100
Last-Translator: 
Language-Team: flabernardez.com
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_x;_ex;_n;_nx;_n_noop;_nx_noop;translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;esc_attr_e;esc_attr_x;number_format_i18n;date_i18n
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
X-Poedit-SearchPathExcluded-1: scss
X-Poedit-SearchPathExcluded-2: config.codekit3
X-Poedit-SearchPathExcluded-3: screenshot.png
 %1$s comentarios en &ldquo;%2$s&rdquo; Añadir widgets aquí. Blog Comentarios cerrados. Continúa leyendo<span class=“screen-reader-text”> “%s”</span> Error 404 Destacado Pie de página Cabecera Página de inicio Un comentario en &ldquo;%1$s&rdquo; Páginas: Busca… Busca: Resultados de la búsqueda para: Lo siento, pero no hemos encontrado coincidencia con los términos de búsqueda. Prueba de nuevo con diferentes palabras claves. ¡Anda! Esta página no existe. Prueba a %1$s o a continuación te dejo contenido interesante ya creado: visita la página de inicio 