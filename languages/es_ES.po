msgid ""
msgstr ""
"Project-Id-Version: Cancer Theme\n"
"POT-Creation-Date: 2019-01-23 10:41+0100\n"
"PO-Revision-Date: 2019-01-23 10:41+0100\n"
"Last-Translator: \n"
"Language-Team: flabernardez.com\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;_x;_ex;_n;_nx;_n_noop;_nx_noop;"
"translate_nooped_plural;esc_html__;esc_html_e;esc_html_x;esc_attr__;"
"esc_attr_e;esc_attr_x;number_format_i18n;date_i18n\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: js\n"
"X-Poedit-SearchPathExcluded-1: scss\n"
"X-Poedit-SearchPathExcluded-2: config.codekit3\n"
"X-Poedit-SearchPathExcluded-3: screenshot.png\n"

#: 404.php:17
#, php-format
msgid ""
"This page does not exist. Please, try to %1$s or we let you content that is "
"already created:"
msgstr ""
"¡Anda! Esta página no existe. Prueba a %1$s o a continuación te dejo "
"contenido interesante ya creado:"

#: 404.php:21
msgid "Home page"
msgstr "Página de inicio"

#: 404.php:22
msgid "visit home page"
msgstr "visita la página de inicio"

#: comments.php:34
#, php-format
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr "Un comentario en &ldquo;%1$s&rdquo;"

#: comments.php:40
#, php-format
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgstr "%1$s comentarios en &ldquo;%2$s&rdquo;"

#: comments.php:63
msgid "Comments are closed."
msgstr "Comentarios cerrados."

#: functions.php:20
msgid "Header"
msgstr "Cabecera"

#: functions.php:24 functions.php:76
msgid "Footer"
msgstr "Pie de página"

#: functions.php:78
msgid "Add widgets here."
msgstr "Añadir widgets aquí."

#: header.php:62
msgid "Blog"
msgstr "Blog"

#: header.php:67
msgid "Error 404"
msgstr "Error 404"

#: header.php:72
msgid "Search results for:"
msgstr "Resultados de la búsqueda para:"

#: index.php:22
#, php-format
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr "Continúa leyendo<span class=“screen-reader-text”> “%s”</span>"

#: index.php:33
msgid "Pages:"
msgstr "Páginas:"

#: search.php:20
msgid "Featured"
msgstr "Destacado"

#: search.php:42
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""
"Lo siento, pero no hemos encontrado coincidencia con los términos de "
"búsqueda. Prueba de nuevo con diferentes palabras claves."

#: searchform.php:12
msgid "Search for:"
msgstr "Busca:"

#: searchform.php:13
msgid "Search for..."
msgstr "Busca…"

#~ msgid ""
#~ "¡Anda! Esta página no existe. Prueba a %1$s o a continuación te dejo "
#~ "contenido interesante ya creado:"
#~ msgstr ""
#~ "¡Anda! Esta página no existe. Prueba a %1$s o a continuación te dejo "
#~ "contenido interesante ya creado:"

#~ msgid "Home de la web"
#~ msgstr "Home de la web"

#~ msgid "visitar la página de inicio de esta web"
#~ msgstr "visitar la página de inicio de esta web"
